﻿#include <iostream>

using namespace std;

class Stack
{
private:
    int* arr;
    int size = 0;
public:
    void showMenu()
    {
        cout << "Choose:" << '\n';
        cout << "1. Push element" << '\n';
        cout << "2. Pop element" << '\n';
    }

    void pushElement()
    {
        int num;
        cout << "Enter a number: ";
        cin >> num;
        push(num);
    }
   
    void push(int elem)
    {
        size++;
        int* newArr = new int[size];
        *newArr = elem;
        for (int i = 1; i < size; i++)
        {
            *(newArr + i) = *(arr + i - 1);
        }
        deleteArray();
        arr = newArr;
    }

    void popElement()
    {
        if (size == 0)
        {
            cout << "Empty array" << '\n';
        }
        else
        {
            cout << "Element: " << pop() << '\n';
        }
    }
   
    int pop()
    {
        size--;
        int* newArr = new int[size];
        int responce = *arr;
        for (int i = 1; i <= size; i++)
        {
            *(newArr + i - 1) = *(arr + i);
        }
        deleteArray();
        arr = newArr;
        return responce;
    }

    void deleteArray()
    {
        delete arr;
    }
};

int main()
{
    Stack stack;
    int actionNum;
    bool menuOn = true;
    while (menuOn)
    {
        stack.showMenu();
        cin >> actionNum;
        switch (actionNum)
        {
        case 1:
            stack.pushElement();
            break;
        case 2:
            stack.popElement();
            break;
        default:
            cout << "Invalid input!" << '\n';
            break;
        }
    }
}